import React, { Component, useState } from 'react';
import '../source/Stylesheet.css';
import Header from '../source/Component/Header';
import TableHeader from '../source/Component/TableHeader';
import { AiOutlinePlusCircle } from "react-icons/ai";
import Modal from 'react-awesome-modal';
import "react-datepicker/dist/react-datepicker.css";
import Moment from "moment";
import { MdRadioButtonUnchecked } from "react-icons/md";
import { MdRadioButtonChecked } from "react-icons/md";
import DateFnsUtils from '@date-io/date-fns'; // choose your lib

import { MdDelete } from "react-icons/md";
import { MdModeEdit } from "react-icons/md";
import {
    DatePicker,
    TimePicker,
    DateTimePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';
import { keys } from '@material-ui/core/styles/createBreakpoints';
const list = [
    {
        id: 1,
        image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAeFBMVEX///8AAADZ2dn7+/vR0dHu7u5qamrz8/OhoaHq6ur29vZnZ2fLy8u9vb2zs7P5+fmPj4+Hh4ddXV0TExPj4+Onp6dFRUVSUlJ0dHRISEgwMDDAwMAmJiaBgYEJCQl6enozMzMfHx+Xl5c8PDyTk5NPT08xMTFgYGDhQRb5AAAI9ElEQVR4nO2d6ZaqOhCFxaEFZ8VZnD3X93/DuxBtQaYke5fQa+X73R1TkNSUStFoWCwWi8VisVgsFovFYrHo0oqoehpkut586E72x9nyFJx9x3H8c3Bazo77iTuce92qpwfRa+6m903gFBFs7tNds1f1VA2Yj9bbQ6FscQ7b9Whe9ZTVac3dvrJscfru/A9sUW933BqJF7E97ryqRSiiO1oB0r1YjWqqfnoU8V5C1k/3tI808SKO7apFitNxr2T5Qq5up2rBnnjs1/fmWAe107yLyRdyb1Ys34CnXfJYDSqUry0v30PGqpRO8zvyPWSsYq125PRLFsev61X3q/KFuF+Vb4D4nqZsv6dyeusK5AtZf8mXG1YkX8jwC/J1fyoU0HF+xOOOgV+pgI7jC+/GacXyhUwF5ZtLhBD6XMVSOruqRftlJyPgpGq5YkwE5OvNqpYqwYxuGr3ixO73CcjBcbNqI5HGp8YbVbox+RAdnEXVsuSwYAn4/UhJFVJEdalajgIuDAHr4KjlQ3Dh6i0gQcQ6L9EIcKHWV8m8gdRNXc1EEsBo1CeYKMY41GhWPXNlDB04T73SoGoORm74uIqcqCnbsYGE/Hhwud5PJ5fJdL9e0see6Qt4o07gehkmF5I3vHCTPjddAYnx0qE/zD5W6Qz7xK2uGUvNaT98KjyNZ1YA6GXgWFpmWZ6/HbA25VZHwD3pN9VWzo70PPfqApJ8GXVviuQdKvs23TPj51Y6VtijnJifVY9tzCoLP9D1+ClRTF/ttyiGQv9ogaK+lTb+mPBDS5O6gg5Dqap4b4Qj7L5ZIWyLsD3W5T8zwH/lx0i+EML5cqkBbuG2yVxAhojbsvWDqzREQIaIJUq8C//ABhKw0djAMyg2iriaQc/1evAMCpUNnpnBj7xk5wDH9SNYwEZjhE6iIN5vo2Mrek0lwGYxvx4Vdn85BUuwulvljQwbe9ahLOwZ55l91C80yHflgOqDZfaw8C7klSrBcUb2TkQfnEYSoRQ0jZK5nODnxixw8dDJZK0n9LFxC7HQErOMBdUBh3S4xfQC00GDCiykSIMGGekQ4wSOyL7zgbqnp88BUVORY4EAUOv8aTDQRUErwPoFzRJ/bBs4KuNfFIQNRjJSRSOWXF8XAI0DRtTR+IsUX6aJpy7iQqBQnSz0cWkd3SmDJjbjCwt1ujmx/SdorB9zv2EXiZGeSQMnbN6eGxxTy1xihbNu75wDfOFVRMBGA53WkTaSLyQhfAXiNRC8GngJmiRw+va1e+DTGIlbSCHwTatXCAWnSSU8mhC4ROPl1sClV0KX5fCql0M0Dn4UInVxFT+PjjYibFiFzCHj2UeuCH7sKnVrFS9AicJgvMhD6gY5fiL9SK508KuFUq0O8PNgP3RNCbVI9X2Hjw2EKxqBJE0EHJhHqoZwdau+muZx8YtQaiXV5Qg+74tic0KhtUwATNlAV04polT7H0bN6ZigkUl3VTNg3PnoUa5v3YUkZDS7a1Iqgg9CEjLumww5ZfJCEjKmtuBUkNc01xbicnqy1DRfGjJhlHXXNef9YM25ZhiISEhpGDNjuDSOjN/GuYZ8Jd1Sk+i/xWl2sOUsBeck0Gr8H2VmQeNMGUfAXpAueZ7xs4EIftqb1JHDp3hGjoQ2JbXeOlA8oxB2VpgQ/Uaw3iG9KIrVDuDA2odsk0i7TO7zJOQWDdHa3PqNM2so6kvktVU5s1SWw60w5TW6DUh+6QPeS6Qp0tAvJTZW5xUsELs0rojrged/k1o6PFhz+7FxvFPiGg3dSWq/sitDwBZacp7A5SRDfmE44NwufwvG7fQ4eE6K3Ct1yDijS4CaDF7vn4gm5WQmAXaYSDhzT9JrtNgtkAPkzLtL1TIhY4GuZUvzwoUWvc3ZqUEoLk0RmF7yGvO72ofOMrcxW4SZ5ZfoGR6aL65BfGKS02hL9AwPrZdMh0t9uyjypB+ria6fI370csRjmU+fRE0xObnlFGedlSr16ZMoPybW7XmtqlM9sY8PRX1OBHvpTlVMY0tCmz+J1AHbE0wwLXuPHdGG4U+zdZb8DefYzu8wNqZ/3POD5++Q1Ng1z6fc7rMDjvZeupHvK4fLKDi5ul7RtQZ/c0m0afWGl3/56nMzIvmnr2sSsM1fLp6zL1lzh9PqZ/+zOpWYhlABegu8JVbMe4RGOd1iXigj6/OqA5zf4EX8Oy9gu88GSd8Fz5PFHIXWACs4eXf9Ms7VTNP1z2NsUrOP+BkyJu/7hz0jnylws80A4kJnOOw913ixxp6Wgb0IFrlmzjM1P7PsHE9rdDYbLjaGfgZvURg6tE0ee5DvqreMDFr8Sl1H86x7WhoaLXTjdb/kip+B85pYEVrpqB+VhGFrpJN0uY5Km8fOdVVY8lBaQzv4yrcNd6ond//UhtzprYuk1lKvZ9/rxO4dt3xSwUX9yo3WsdvHRBVjUF87ad8sckD9zUQvLTdQf42fh+5qvqnZl1w77Uk/HXf8d5+09ROr6t8iTmlmFUcXuNHc6s6H7uU2Xd/X09vFHc67ptWMiioj3YxE4R+r+MB5Bs2zioTpt9Er+7/N179unkdXJTuYMdsSF1fqXowR5ZdpspoJFyekZKrxjSmN9zJ3VJF9Zva1pFByTSS7U0dB8Fo7Acs2VY5SzH2J7J6IFIoWal6xa57Vl2jGRqDAE8+Nw7L/h98SkUP+kXj+K8l+iWJfMkfJ/fRdgfOc9RKlWrMQyNGNRS2PMmyiVP8gCtk2o9C7THnuMt30aGRtxeIPsaRqwKSaCZDI0hwlEd5HWr6Gpj5J2vCXNgdIvnepriU0UmV5p9J/Sbx37c8mfp/PTKpCfUQ82yM/QZyzhpp58v5zqW4QVJKaQyk38q7NqE1UX0Q37tkofmHj5dnUMqRIE9tWqnH66xRDqtEcmbfv5iur/ujENJBq4cXmN0us8Upuf2iRvpeplnuyUd+11bNTtfVxwq1Y27jwk+hY6aCp+QdibXUEeEioHSNMKHeYvkNYN2BwpiLVokyAi6K39ndZwF9drDvDv+FfArT/iPdlsVgsFovFYrFYLBaLxVLA/yDelsc/kt8ZAAAAAElFTkSuQmCC",
        imgfile:'',
        firstname: "Avishkar",
        fathername: "Shantaram",
        lastname: "Patil",
        email: "avi@gmail.com",
        address: "at-jogalkheda post-nachankheda tal-jamner dist-jalgaon",
        mobile: "8411979854",
        gender: "Male",
        DOB: "02/12/1997",
        country: "India",
    },
    {
        id: 2,
        image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAeFBMVEX///8AAADZ2dn7+/vR0dHu7u5qamrz8/OhoaHq6ur29vZnZ2fLy8u9vb2zs7P5+fmPj4+Hh4ddXV0TExPj4+Onp6dFRUVSUlJ0dHRISEgwMDDAwMAmJiaBgYEJCQl6enozMzMfHx+Xl5c8PDyTk5NPT08xMTFgYGDhQRb5AAAI9ElEQVR4nO2d6ZaqOhCFxaEFZ8VZnD3X93/DuxBtQaYke5fQa+X73R1TkNSUStFoWCwWi8VisVgsFovFYrHo0oqoehpkut586E72x9nyFJx9x3H8c3Bazo77iTuce92qpwfRa+6m903gFBFs7tNds1f1VA2Yj9bbQ6FscQ7b9Whe9ZTVac3dvrJscfru/A9sUW933BqJF7E97ryqRSiiO1oB0r1YjWqqfnoU8V5C1k/3tI808SKO7apFitNxr2T5Qq5up2rBnnjs1/fmWAe107yLyRdyb1Ys34CnXfJYDSqUry0v30PGqpRO8zvyPWSsYq125PRLFsev61X3q/KFuF+Vb4D4nqZsv6dyeusK5AtZf8mXG1YkX8jwC/J1fyoU0HF+xOOOgV+pgI7jC+/GacXyhUwF5ZtLhBD6XMVSOruqRftlJyPgpGq5YkwE5OvNqpYqwYxuGr3ixO73CcjBcbNqI5HGp8YbVbox+RAdnEXVsuSwYAn4/UhJFVJEdalajgIuDAHr4KjlQ3Dh6i0gQcQ6L9EIcKHWV8m8gdRNXc1EEsBo1CeYKMY41GhWPXNlDB04T73SoGoORm74uIqcqCnbsYGE/Hhwud5PJ5fJdL9e0see6Qt4o07gehkmF5I3vHCTPjddAYnx0qE/zD5W6Qz7xK2uGUvNaT98KjyNZ1YA6GXgWFpmWZ6/HbA25VZHwD3pN9VWzo70PPfqApJ8GXVviuQdKvs23TPj51Y6VtijnJifVY9tzCoLP9D1+ClRTF/ttyiGQv9ogaK+lTb+mPBDS5O6gg5Dqap4b4Qj7L5ZIWyLsD3W5T8zwH/lx0i+EML5cqkBbuG2yVxAhojbsvWDqzREQIaIJUq8C//ABhKw0djAMyg2iriaQc/1evAMCpUNnpnBj7xk5wDH9SNYwEZjhE6iIN5vo2Mrek0lwGYxvx4Vdn85BUuwulvljQwbe9ahLOwZ55l91C80yHflgOqDZfaw8C7klSrBcUb2TkQfnEYSoRQ0jZK5nODnxixw8dDJZK0n9LFxC7HQErOMBdUBh3S4xfQC00GDCiykSIMGGekQ4wSOyL7zgbqnp88BUVORY4EAUOv8aTDQRUErwPoFzRJ/bBs4KuNfFIQNRjJSRSOWXF8XAI0DRtTR+IsUX6aJpy7iQqBQnSz0cWkd3SmDJjbjCwt1ujmx/SdorB9zv2EXiZGeSQMnbN6eGxxTy1xihbNu75wDfOFVRMBGA53WkTaSLyQhfAXiNRC8GngJmiRw+va1e+DTGIlbSCHwTatXCAWnSSU8mhC4ROPl1sClV0KX5fCql0M0Dn4UInVxFT+PjjYibFiFzCHj2UeuCH7sKnVrFS9AicJgvMhD6gY5fiL9SK508KuFUq0O8PNgP3RNCbVI9X2Hjw2EKxqBJE0EHJhHqoZwdau+muZx8YtQaiXV5Qg+74tic0KhtUwATNlAV04polT7H0bN6ZigkUl3VTNg3PnoUa5v3YUkZDS7a1Iqgg9CEjLumww5ZfJCEjKmtuBUkNc01xbicnqy1DRfGjJhlHXXNef9YM25ZhiISEhpGDNjuDSOjN/GuYZ8Jd1Sk+i/xWl2sOUsBeck0Gr8H2VmQeNMGUfAXpAueZ7xs4EIftqb1JHDp3hGjoQ2JbXeOlA8oxB2VpgQ/Uaw3iG9KIrVDuDA2odsk0i7TO7zJOQWDdHa3PqNM2so6kvktVU5s1SWw60w5TW6DUh+6QPeS6Qp0tAvJTZW5xUsELs0rojrged/k1o6PFhz+7FxvFPiGg3dSWq/sitDwBZacp7A5SRDfmE44NwufwvG7fQ4eE6K3Ct1yDijS4CaDF7vn4gm5WQmAXaYSDhzT9JrtNgtkAPkzLtL1TIhY4GuZUvzwoUWvc3ZqUEoLk0RmF7yGvO72ofOMrcxW4SZ5ZfoGR6aL65BfGKS02hL9AwPrZdMh0t9uyjypB+ria6fI370csRjmU+fRE0xObnlFGedlSr16ZMoPybW7XmtqlM9sY8PRX1OBHvpTlVMY0tCmz+J1AHbE0wwLXuPHdGG4U+zdZb8DefYzu8wNqZ/3POD5++Q1Ng1z6fc7rMDjvZeupHvK4fLKDi5ul7RtQZ/c0m0afWGl3/56nMzIvmnr2sSsM1fLp6zL1lzh9PqZ/+zOpWYhlABegu8JVbMe4RGOd1iXigj6/OqA5zf4EX8Oy9gu88GSd8Fz5PFHIXWACs4eXf9Ms7VTNP1z2NsUrOP+BkyJu/7hz0jnylws80A4kJnOOw913ixxp6Wgb0IFrlmzjM1P7PsHE9rdDYbLjaGfgZvURg6tE0ee5DvqreMDFr8Sl1H86x7WhoaLXTjdb/kip+B85pYEVrpqB+VhGFrpJN0uY5Km8fOdVVY8lBaQzv4yrcNd6ond//UhtzprYuk1lKvZ9/rxO4dt3xSwUX9yo3WsdvHRBVjUF87ad8sckD9zUQvLTdQf42fh+5qvqnZl1w77Uk/HXf8d5+09ROr6t8iTmlmFUcXuNHc6s6H7uU2Xd/X09vFHc67ptWMiioj3YxE4R+r+MB5Bs2zioTpt9Er+7/N179unkdXJTuYMdsSF1fqXowR5ZdpspoJFyekZKrxjSmN9zJ3VJF9Zva1pFByTSS7U0dB8Fo7Acs2VY5SzH2J7J6IFIoWal6xa57Vl2jGRqDAE8+Nw7L/h98SkUP+kXj+K8l+iWJfMkfJ/fRdgfOc9RKlWrMQyNGNRS2PMmyiVP8gCtk2o9C7THnuMt30aGRtxeIPsaRqwKSaCZDI0hwlEd5HWr6Gpj5J2vCXNgdIvnepriU0UmV5p9J/Sbx37c8mfp/PTKpCfUQ82yM/QZyzhpp58v5zqW4QVJKaQyk38q7NqE1UX0Q37tkofmHj5dnUMqRIE9tWqnH66xRDqtEcmbfv5iur/ujENJBq4cXmN0us8Upuf2iRvpeplnuyUd+11bNTtfVxwq1Y27jwk+hY6aCp+QdibXUEeEioHSNMKHeYvkNYN2BwpiLVokyAi6K39ndZwF9drDvDv+FfArT/iPdlsVgsFovFYrFYLBaLxVLA/yDelsc/kt8ZAAAAAElFTkSuQmCC",
        imgfile:'',
        firstname: "Donald",
        fathername: "y",
        lastname: "Trump",
        email: "Pm@gmail.com",
        address: "America",
        mobile: "8411979854",
        gender: "MALE",
        DOB: "02/12/1997",
        country: "India",
    },
    {
        id: 3,
        image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAeFBMVEX///8AAADZ2dn7+/vR0dHu7u5qamrz8/OhoaHq6ur29vZnZ2fLy8u9vb2zs7P5+fmPj4+Hh4ddXV0TExPj4+Onp6dFRUVSUlJ0dHRISEgwMDDAwMAmJiaBgYEJCQl6enozMzMfHx+Xl5c8PDyTk5NPT08xMTFgYGDhQRb5AAAI9ElEQVR4nO2d6ZaqOhCFxaEFZ8VZnD3X93/DuxBtQaYke5fQa+X73R1TkNSUStFoWCwWi8VisVgsFovFYrHo0oqoehpkut586E72x9nyFJx9x3H8c3Bazo77iTuce92qpwfRa+6m903gFBFs7tNds1f1VA2Yj9bbQ6FscQ7b9Whe9ZTVac3dvrJscfru/A9sUW933BqJF7E97ryqRSiiO1oB0r1YjWqqfnoU8V5C1k/3tI808SKO7apFitNxr2T5Qq5up2rBnnjs1/fmWAe107yLyRdyb1Ys34CnXfJYDSqUry0v30PGqpRO8zvyPWSsYq125PRLFsev61X3q/KFuF+Vb4D4nqZsv6dyeusK5AtZf8mXG1YkX8jwC/J1fyoU0HF+xOOOgV+pgI7jC+/GacXyhUwF5ZtLhBD6XMVSOruqRftlJyPgpGq5YkwE5OvNqpYqwYxuGr3ixO73CcjBcbNqI5HGp8YbVbox+RAdnEXVsuSwYAn4/UhJFVJEdalajgIuDAHr4KjlQ3Dh6i0gQcQ6L9EIcKHWV8m8gdRNXc1EEsBo1CeYKMY41GhWPXNlDB04T73SoGoORm74uIqcqCnbsYGE/Hhwud5PJ5fJdL9e0see6Qt4o07gehkmF5I3vHCTPjddAYnx0qE/zD5W6Qz7xK2uGUvNaT98KjyNZ1YA6GXgWFpmWZ6/HbA25VZHwD3pN9VWzo70PPfqApJ8GXVviuQdKvs23TPj51Y6VtijnJifVY9tzCoLP9D1+ClRTF/ttyiGQv9ogaK+lTb+mPBDS5O6gg5Dqap4b4Qj7L5ZIWyLsD3W5T8zwH/lx0i+EML5cqkBbuG2yVxAhojbsvWDqzREQIaIJUq8C//ABhKw0djAMyg2iriaQc/1evAMCpUNnpnBj7xk5wDH9SNYwEZjhE6iIN5vo2Mrek0lwGYxvx4Vdn85BUuwulvljQwbe9ahLOwZ55l91C80yHflgOqDZfaw8C7klSrBcUb2TkQfnEYSoRQ0jZK5nODnxixw8dDJZK0n9LFxC7HQErOMBdUBh3S4xfQC00GDCiykSIMGGekQ4wSOyL7zgbqnp88BUVORY4EAUOv8aTDQRUErwPoFzRJ/bBs4KuNfFIQNRjJSRSOWXF8XAI0DRtTR+IsUX6aJpy7iQqBQnSz0cWkd3SmDJjbjCwt1ujmx/SdorB9zv2EXiZGeSQMnbN6eGxxTy1xihbNu75wDfOFVRMBGA53WkTaSLyQhfAXiNRC8GngJmiRw+va1e+DTGIlbSCHwTatXCAWnSSU8mhC4ROPl1sClV0KX5fCql0M0Dn4UInVxFT+PjjYibFiFzCHj2UeuCH7sKnVrFS9AicJgvMhD6gY5fiL9SK508KuFUq0O8PNgP3RNCbVI9X2Hjw2EKxqBJE0EHJhHqoZwdau+muZx8YtQaiXV5Qg+74tic0KhtUwATNlAV04polT7H0bN6ZigkUl3VTNg3PnoUa5v3YUkZDS7a1Iqgg9CEjLumww5ZfJCEjKmtuBUkNc01xbicnqy1DRfGjJhlHXXNef9YM25ZhiISEhpGDNjuDSOjN/GuYZ8Jd1Sk+i/xWl2sOUsBeck0Gr8H2VmQeNMGUfAXpAueZ7xs4EIftqb1JHDp3hGjoQ2JbXeOlA8oxB2VpgQ/Uaw3iG9KIrVDuDA2odsk0i7TO7zJOQWDdHa3PqNM2so6kvktVU5s1SWw60w5TW6DUh+6QPeS6Qp0tAvJTZW5xUsELs0rojrged/k1o6PFhz+7FxvFPiGg3dSWq/sitDwBZacp7A5SRDfmE44NwufwvG7fQ4eE6K3Ct1yDijS4CaDF7vn4gm5WQmAXaYSDhzT9JrtNgtkAPkzLtL1TIhY4GuZUvzwoUWvc3ZqUEoLk0RmF7yGvO72ofOMrcxW4SZ5ZfoGR6aL65BfGKS02hL9AwPrZdMh0t9uyjypB+ria6fI370csRjmU+fRE0xObnlFGedlSr16ZMoPybW7XmtqlM9sY8PRX1OBHvpTlVMY0tCmz+J1AHbE0wwLXuPHdGG4U+zdZb8DefYzu8wNqZ/3POD5++Q1Ng1z6fc7rMDjvZeupHvK4fLKDi5ul7RtQZ/c0m0afWGl3/56nMzIvmnr2sSsM1fLp6zL1lzh9PqZ/+zOpWYhlABegu8JVbMe4RGOd1iXigj6/OqA5zf4EX8Oy9gu88GSd8Fz5PFHIXWACs4eXf9Ms7VTNP1z2NsUrOP+BkyJu/7hz0jnylws80A4kJnOOw913ixxp6Wgb0IFrlmzjM1P7PsHE9rdDYbLjaGfgZvURg6tE0ee5DvqreMDFr8Sl1H86x7WhoaLXTjdb/kip+B85pYEVrpqB+VhGFrpJN0uY5Km8fOdVVY8lBaQzv4yrcNd6ond//UhtzprYuk1lKvZ9/rxO4dt3xSwUX9yo3WsdvHRBVjUF87ad8sckD9zUQvLTdQf42fh+5qvqnZl1w77Uk/HXf8d5+09ROr6t8iTmlmFUcXuNHc6s6H7uU2Xd/X09vFHc67ptWMiioj3YxE4R+r+MB5Bs2zioTpt9Er+7/N179unkdXJTuYMdsSF1fqXowR5ZdpspoJFyekZKrxjSmN9zJ3VJF9Zva1pFByTSS7U0dB8Fo7Acs2VY5SzH2J7J6IFIoWal6xa57Vl2jGRqDAE8+Nw7L/h98SkUP+kXj+K8l+iWJfMkfJ/fRdgfOc9RKlWrMQyNGNRS2PMmyiVP8gCtk2o9C7THnuMt30aGRtxeIPsaRqwKSaCZDI0hwlEd5HWr6Gpj5J2vCXNgdIvnepriU0UmV5p9J/Sbx37c8mfp/PTKpCfUQ82yM/QZyzhpp58v5zqW4QVJKaQyk38q7NqE1UX0Q37tkofmHj5dnUMqRIE9tWqnH66xRDqtEcmbfv5iur/ujENJBq4cXmN0us8Upuf2iRvpeplnuyUd+11bNTtfVxwq1Y27jwk+hY6aCp+QdibXUEeEioHSNMKHeYvkNYN2BwpiLVokyAi6K39ndZwF9drDvDv+FfArT/iPdlsVgsFovFYrFYLBaLxVLA/yDelsc/kt8ZAAAAAElFTkSuQmCC",
        imgfile:'',
        firstname: "Narendra",
        fathername: "D",
        lastname: "Modi",
        email: "PM@gmail.com",
        address: "Lal Killa Dilhi",
        mobile: "8459242848",
        gender: "MALE",
        DOB: "06/10/1945",
        country: "India",
    },
]
export default class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            newaddeddata: '',
            jsondata: list,
            visible: false,

            image: '',
            imgfile:'',
            fn: '',
            father: '',
            ln: '',
            email: '',
            address: '',
            mobile: '',
            gender: 'MALE',
            dob: String(new Date()),
            country: '',
            errorstate: '',
            submit: true,
            imageerr: '',
            fnerr: '',
            fathererr: '',
            lnerr: '',
            emailerr: '',
            addresserr: '',
            mobileerr: '',
            gendererr: '',
            // dob: new Date(),
            countryerr: '',
            editflag: false,
            editkey: '',

            file: '',
            imagePreviewUrl: '',
            male:true,
            female:false,

        }
        // this.handleChangeimg = this.handleChangeimg.bind(this);
        this.handleChangefn = this.handleChangefn.bind(this);
        this.handleChangefather = this.handleChangefather.bind(this);
        this.handleChangeln = this.handleChangeln.bind(this);
        this.handleChangeemail = this.handleChangeemail.bind(this);
        this.handleChangeaddress = this.handleChangeaddress.bind(this);
        this.handleChangemobile = this.handleChangemobile.bind(this);
        // this.handleChangecountry = this.handleChangecountry.bind(this);

        this._handleImageChange = this._handleImageChange.bind(this);
        this._handleSubmit = this._handleSubmit.bind(this);

    }

    _handleSubmit(e) {
        e.preventDefault();
        // TODO: do something with -> this.state.file
        // this.setState({file:'',imagePreviewUrl:''})
    }

    _handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    componentDidMount() {
        var date = new Date()
        var day = String(Moment(date).format('DD'))
        var mon = String(Moment(date).format('MMMM'))
        var year = String(Moment(date).format('YYYY'))
        this.setState({ dob: day + "/" + mon + '/' + year })
    }


    handleChangefn(event) {
        this.setState({ fn: event.target.value, fnerr: '' });
    }

    handleChangefather(event) {
        this.setState({ father: event.target.value, fathererr: '' });
    }

    handleChangeln(event) {
        this.setState({ ln: event.target.value, lnerr: '' });
    }

    handleChangeemail(event) {
        this.setState({ email: event.target.value, emailerr: '' });
    }

    handleChangeaddress(event) {
        this.setState({ address: event.target.value });
    }

    handleChangemobile(event) {
        this.setState({ mobile: event.target.value, mobileerr: '' });
    }

    setGender(event) {
        this.setState({ gender: event.target.value, gendererr: '' })
    }

    handleChangeBirth = date => {
        var day = String(Moment(date).format('DD'))
        var mon = String(Moment(date).format('MMMM'))
        var year = String(Moment(date).format('YYYY'))
        console.log("date formated==", day, mon, year)
        this.setState({ dob: day + "/" + mon + '/' + year })
        console.log("date formated state==", this.state.dob)
    };

    addstudent() {
        if (this.state.submit == true) {
            console.log("addds  student call");
            var data = {
                image:this.state.imagePreviewUrl,
                firstname: this.state.fn,
                fathername: this.state.father,
                lastname: this.state.ln,
                email: this.state.email,
                address: this.state.address,
                mobile: this.state.mobile,
                gender: this.state.gender,
                DOB: this.state.dob,
                country: "India",
            }

            console.log("image url==",data);

            if (this.state.editflag == false) {
                list.push(data);
                this.setState({ jsondata: list, visible: false });
                this.setState({
                    image: '',
                    fn: '',
                    father: '',
                    ln: '',
                    email: '',
                    address: '',
                    mobile: '',
                    // gender: '',
                    // dob: new Date(),
                    country: '',
                    // imagePreviewUrl:'',
                    // file:'',
                })
            }

            if (this.state.editflag == true) {
                var obj = this.state.jsondata;
                obj[this.state.editkey] = data;
                this.setState({ visible: false, jsondata: obj })
                this.setState({
                    image: '',
                    fn: '',
                    father: '',
                    ln: '',
                    email: '',
                    address: '',
                    mobile: '',
                    // gender: '',
                    // dob: new Date(),
                    country: '',
                    editflag: false,
                    editkey: '',
                    // imagePreviewUrl:'',
                    // file:'',
                })

            }

        }

    }

    canclemodal() {
        this.setState({ visible: false })
    }

    modalopen() {
        this.setState({ visible: true })
    }


    validate() {
        this.state.submit = true;
        if (this.state.fn === "" || this.state.fn === " ") {
            this.setState({ fnerr: "Enter First Name Field Data" })
            this.state.submit = false;
        }
        if (this.state.father === "" || this.state.father === " ") {
            this.setState({ fathererr: "Enter Middle Name Field Data" })
            this.state.submit = false;
        }
        if (this.state.ln === "" || this.state.ln === " ") {
            this.setState({ lnerr: "Enter Last Name Field Data" })
            this.state.submit = false;
        }

        if (this.state.email !== '') {
            var regExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (!regExp.test(this.state.email || this.state.email.trim() === '') || /^\s/.test(this.state.email)) {
                this.state.submit = false
                this.setState({ emailerr: 'Enter valid email' })
            }
        } else {
            this.state.submit = false
            this.setState({ emailerr: 'Enter email' })
        }


        if (this.state.mobile !== '') {
            var mobileno = this.state.mobile;
            var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
            if (mobileno.match(phoneno)) {
                // this.setState({mobileerr:'Valid no'})
                // this.state.submit = true
            }
            else {
                this.setState({ mobileerr: 'Enter valid mobile no' })
                this.state.submit = false
            }
        } else {
            this.state.submit = false
            this.setState({ mobileerr: 'Enter Mobile number' })
        }

        if (this.state.gender === "" || this.state.gender === " ") {
            this.setState({ gendererr: "Please Select Gender" })
            this.state.submit = false;
        }

        if (this.state.submit == true) {
            console.log("submit ===", this.state.submit)
            this.addstudent()
        }

    }

    remove(data, key) {
        var obj = this.state.jsondata;
        obj.splice(key, 1);
        console.log("after delete json data", obj)
        this.setState({ jsondata: obj })
    }

    edit(data, key) {
        this.setState({
            // image: '',
            fn: data.firstname,
            father: data.fathername,
            ln: data.lastname,
            email: data.email,
            address: data.address,
            mobile: data.mobile,
            gender: data.gender,
            dob: data.DOB,
            country: data.country,
        })

        this.setState({ editflag: true, visible: true, editkey: key })
    }


    render() {

        let { imagePreviewUrl } = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (<img src={imagePreviewUrl} />);
        }


        // console.log("name----", this.state.fn)
        return (
            <div class="fullpagediv">
                <Header />

                {/* ==================Modal Data========= */}
                <Modal visible={this.state.visible} width='50%' effect="fadeInUp"
                >

                    <div class="mt-2">
                        <form onSubmit={this._handleSubmit}>
                            <input type="file" onChange={this._handleImageChange} />
                            {/* <button type="submit" onClick={this._handleSubmit}>Upload Image</button> */}
                        </form>
                        {/* {$imagePreview} */}
                    </div>

                    <div class="m-2">

                        <div class="">
                            <input placeholder="First Name *" class=""
                                value={this.state.fn} onChange={this.handleChangefn}
                            />
                            <div class="errorstatetext">{this.state.fnerr}</div>
                        </div>


                        <div class="mt-3">
                            <input placeholder="Middle Name *" class="mx-3"
                                value={this.state.father} onChange={this.handleChangefather}
                            />
                            <div class="errorstatetext">{this.state.fathererr}</div>
                        </div>

                        <div class="mt-3">
                            <input placeholder="Last Name *" class=""
                                value={this.state.ln} onChange={this.handleChangeln}
                            />
                            <div class="errorstatetext">{this.state.lnerr}</div>
                        </div>

                        <div class="mt-3">
                            <input placeholder="E-mail ID *" class=""
                                value={this.state.email} onChange={this.handleChangeemail}
                            />
                            <div class="errorstatetext">{this.state.emailerr}</div>
                        </div>

                        <div class="form-group mt-3 mx-5">
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" placeholder="Address"
                                value={this.state.address} onChange={this.handleChangeaddress}
                            >
                            </textarea>
                            {/* <div class="userregeerrortext">{this.state.addresserror}</div> */}
                        </div>

                        <div class="mt-3">
                            <input placeholder="Mobile Number *" class=""
                                value={this.state.mobile} onChange={this.handleChangemobile}
                            />
                            <div class="errorstatetext">{this.state.mobileerr}</div>
                        </div>


                        <div onChange={this.setGender.bind(this)} class="mt-3"> 

                        {
                        this.state.male
                            ? <MdRadioButtonChecked size={23} color="#206319" />
                            : <MdRadioButtonUnchecked size={23} color="#70707033" onClick={() => this.setState({ male: true, female: false, gender: 'MALE' })} />
                    }
                      <span class="mr-3">MALE</span>
                      {
                        this.state.female
                            ? <MdRadioButtonChecked size={23} color="#206319" />
                            : <MdRadioButtonUnchecked size={23} color="#70707033" onClick={() => this.setState({ male: false, female: true, gender: 'FEMALE' })} />
                    }
                     <span class="mr-3">FEMALE</span>
                            {/* <input type="radio" value="MALE" name="gender" /> Male */}
                            {/* <input type="radio" value="FEMALE" name="gender" class="ml-4" /> Female */}
                            {/* <div class="errorstatetext">{this.state.gendererr}</div> */}
                         </div>

                        <div class="mt-3">
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <DatePicker value={this.state.dob}
                                    onChange={this.handleChangeBirth}
                                    dateFormat="dd/MM/yyyy"
                                />
                            </MuiPickersUtilsProvider>
                        </div>


                        <div class="mt-3">
                            {
                                this.state.editflag
                                    ? <button type="button" class="btn btn-primary mr-5" onClick={() => this.validate()}>
                                        UPDATE
                            </button>
                                    : <button type="button" class="btn btn-primary" onClick={() => this.validate()}>
                                        SUBMIT
                            </button>
                            }



                            <button type="button" class="btn btn-danger ml-5" onClick={() => this.canclemodal()}>
                                CANCEL
                            </button>
                        </div>
                    </div>
                </Modal>


                <button type="button" class="btn btn-primary mt-2" onClick={() => this.modalopen()}>
                    ADD STUDENT
                <AiOutlinePlusCircle size={25} class="ml-2" />
                </button>

                <div class="container-fluid mt-4">
                    <TableHeader />

                    {
                        this.state.jsondata.map((data, key) =>

                            <div class="row border tabletext ">
                                <div class="col border">
                                    {key + 1}
                                </div>

                                <div class="col border">
                                    <img class="imagedisplay"
                                    
                                     src={data.image}    
                                />
                                 {/* <div>
          <form onSubmit={this._handleSubmit}>
            <input type="file" onChange={this._handleImageChange} />
            <button type="submit" onClick={this._handleSubmit}>Upload Image</button>
          </form>
          {$imagePreview}
        </div> */}
                                </div>

                                <div class="col-2 border">
                                    {data.firstname}  {data.fathername} {data.lastname}
                                </div>

                                <div class="col-2 border">
                                    {data.email}
                                </div>
                                <div class="col-2 border">
                                    {data.address}
                                </div>

                                <div class="col border">
                                    {data.mobile}
                                </div>

                                <div class="col border">
                                    {data.gender}
                                </div>

                                <div class="col border">
                                    {data.DOB}
                                </div>

                                <div class="col border" >

                                    <MdDelete size={25} color="#ff0000" class="d-inline-block" onClick={() => this.remove(data, key)} />
                                    <MdModeEdit size={25} color="#006400" class="d-inline-block" onClick={() => this.edit(data, key)} />
                                </div>
                            </div>

                        )
                    }



                </div>
            </div>
        )
    }
}
